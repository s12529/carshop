import java.util.Date;

public interface InterCar {


    String getBrand();
    void setBrand(String brand);
    
	double getCena();
	void setCena(double cena);
	
	long getMileage();
	void setMileage(long mileage);
	
	int getDateOfProduction();
	void setDateOfProduction(int dateOfProduction);
	
	String getCity();
	void setCity(String city);
	
	boolean isDamaged();
	void setDamaged(boolean damaged);
	
	boolean isBrandNew();
	void setBrandNew(boolean brandNew);
	
	Date getData();
	void setData(Date data);
}
