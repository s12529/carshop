import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class CarShop{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Date data = new Date();
		
		List<Car> car = new ArrayList<Car>();

		
		car.add(new Car("Honda", 25000, 0, 2004, "Gdansk", false, false, data));
		car.add(new Car("Mercedes", 9000, 240000, 2001, "Warszawa", true, false, data));
		car.add(new Car("Toyota", 90000, 0, 2013, "Krakow", false, true, data));
		car.add(new Car("Nissan", 43000, 50000, 2006, "Gdansk", false, false, data));
		car.add(new Car("Mitsubishi", 21000, 300000, 1999, "Wroclaw", false, false, data));
		
	
		
		for(int i = 0; i < car.size(); i++ ) {
			System.out.println(car.get(i));
		}
		
		System.out.println("\nWszystkie samochody z miasta Gdansk:\n");
		
		for(int i = 0; i < car.size(); i++ ) {
			if (car.get(i).getCity() == "Gdansk")
			System.out.println(car.get(i));
		}
		

		Collections.sort(car);
		
		System.out.println("\nOgloszenie samochodow wedlug marki (alfabetycznie):\n");
		for(int i = 0; i < car.size(); i++ ) {

			System.out.println(car.get(i));
		}
		
		
		
		
	}

}
