import java.util.Date;


public class Car implements InterCar,Comparable<Car>{
	String brand;
	String city;
	double cena;
	long mileage;
	Integer dateOfProduction;
	boolean damaged;
	boolean brandNew;
	Date data = new Date();
	
	@Override
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public long getMileage() {
		return mileage;
	}
	public void setMileage(long mileage) {
		this.mileage = mileage;
	}
	public int getDateOfProduction() {
		return dateOfProduction;
	}
	public void setDateOfProduction(int dateOfProduction) {
		this.dateOfProduction = dateOfProduction;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public boolean isDamaged() {
		return damaged;
	}
	public void setDamaged(boolean damaged) {
		this.damaged = damaged;
	}
	public boolean isBrandNew() {
		return brandNew;
	}
	public void setBrandNew(boolean brandNew) {
		this.brandNew = brandNew;
	}
	public Date getData() {
		return data;
	}
	
	
	public void setData(Date data) {
		this.data = data;
	}
	
	
	public Car(String brand, double cena, long mileage, int dateOfProduction, String city, boolean damaged, boolean brandNew, Date data) {
		new Date();
		this.brand = brand;
		this.cena = cena;
		this.mileage = mileage;
		this.dateOfProduction = dateOfProduction;
		this.city = city;
		this.damaged = damaged;
		this.brandNew = brandNew;
		this.data = data;
	}
	
	
	@Override
	public String toString() {
		return "Car [brand=" + brand + ", city=" + city + ", cena=" + cena
				+ ", mileage=" + mileage + ", dateOfProduction="
				+ dateOfProduction + ", damaged=" + damaged + ", brandNew="
				+ brandNew + ", data=" + data + "]";
	}
	@Override
	public int compareTo(Car o) {
		// TODO Auto-generated method stub
		Integer sortByBrand = dateOfProduction.compareTo(o.dateOfProduction);
		
		
		return sortByBrand;
	}


    
}



